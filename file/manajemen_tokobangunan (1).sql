-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 22, 2018 at 08:34 PM
-- Server version: 10.0.34-MariaDB-0ubuntu0.16.04.1
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `manajemen_tokobangunan`
--

-- --------------------------------------------------------

--
-- Table structure for table `akses`
--

CREATE TABLE `akses` (
  `id` int(11) NOT NULL,
  `akses` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akses`
--

INSERT INTO `akses` (`id`, `akses`) VALUES
(1, 'Admin Sistem'),
(2, 'Admin Penjualan');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id`, `nama`) VALUES
(16, 'Atap plastik gelombang 80 x 180'),
(17, 'Atap Fiber Glass Tipis 80 x 180 (gelombang)'),
(18, 'Atap Fiber Glass Tebal 80 x 180 (gelombang)'),
(19, 'Atap Alumunium Natural USR 26(JAINDO)'),
(20, 'Atap Alumunium warna USR 26(JAINDO)'),
(21, 'Atap Asbes Gel. Kecil 80x180x4 mm'),
(22, 'Atap Asbes Gel. besar 80x180x5 mm'),
(23, 'Atap Asbes Gel. 300cmx105cmx4 mm'),
(24, 'Atap Seng Gel. 150cmx102cmx4mm'),
(25, 'Atap Seng Gel. 150cmx102cmx5mm'),
(26, 'Atap Seng Gel. 180cmx102cmx5mm'),
(27, 'Atap Seng Gel. 210cmx105cmx4mm'),
(28, 'Atap Seng Gel. 240cmx105cmx4mm'),
(29, 'Atap Seng Gel. 270cmx105cmx4mm'),
(30, 'Atap Seng Gel. 300cmx105cmx4mm'),
(31, 'Aluminium Foile ukuran 1,2 x 50m'),
(32, 'Nok Atas Metal (rainbow roof) uk. 1,1m x 5cm x 6cm'),
(33, 'Nok Atas Bulat 60cm x 14cm'),
(34, 'Nok Prima 90cm x 20cm'),
(35, 'Nok Pinggir Metal (rainbow roof)'),
(36, 'Nok Stel gelombang'),
(37, 'Nok Atap Seng'),
(38, 'Nok Standar 40 cm 18,SWG 22'),
(39, 'Plat Asbes tebal 4mm'),
(40, 'Plat Asbes tebal 3.5mm');

-- --------------------------------------------------------

--
-- Table structure for table `centroid`
--

CREATE TABLE `centroid` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jumlah` varchar(100) NOT NULL,
  `volume` varchar(100) NOT NULL,
  `rerata` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `centroid`
--

INSERT INTO `centroid` (`id`, `nama`, `jumlah`, `volume`, `rerata`) VALUES
(1, 'C1', '8.5', '116.25', '10.215'),
(2, 'C2', '14.25', '241.75', '14.338'),
(3, 'C3', '21.625', '449.625', '32.128');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id` int(11) NOT NULL,
  `barang_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `barang_id`, `jumlah`) VALUES
(8, 16, 14),
(9, 16, 13),
(10, 16, 13),
(11, 16, 14),
(12, 16, 15),
(13, 16, 12),
(14, 16, 12),
(15, 16, 14),
(16, 16, 13),
(17, 16, 15),
(18, 16, 14),
(19, 16, 9),
(20, 16, 12),
(21, 16, 12),
(22, 16, 15),
(23, 16, 10),
(24, 16, 12),
(25, 16, 15),
(26, 16, 10),
(27, 16, 13),
(28, 16, 15),
(29, 16, 16),
(30, 16, 18),
(31, 17, 12),
(32, 17, 13),
(33, 17, 10),
(34, 17, 9),
(35, 17, 13),
(36, 17, 18),
(37, 17, 17),
(38, 17, 15),
(39, 17, 14),
(40, 17, 18),
(41, 17, 17),
(42, 18, 10),
(43, 18, 10),
(44, 18, 11),
(45, 18, 8),
(46, 18, 12),
(47, 18, 11),
(48, 18, 9),
(49, 18, 10),
(50, 18, 16),
(51, 19, 17),
(52, 19, 16),
(53, 19, 18),
(54, 19, 19),
(55, 19, 16),
(56, 19, 16),
(57, 19, 15),
(58, 19, 19),
(59, 19, 20),
(60, 19, 15),
(61, 19, 21),
(62, 19, 14),
(63, 19, 16),
(64, 19, 15),
(65, 19, 15),
(66, 19, 14),
(67, 19, 10),
(68, 20, 12),
(69, 20, 13),
(70, 20, 13),
(71, 20, 12),
(72, 20, 10),
(73, 20, 14),
(74, 20, 12),
(75, 20, 13),
(76, 20, 10),
(77, 20, 13),
(78, 20, 13),
(79, 20, 13),
(80, 20, 14),
(81, 20, 12),
(82, 20, 13),
(83, 20, 12),
(84, 20, 14),
(85, 20, 11),
(86, 21, 19),
(87, 21, 20),
(88, 21, 22),
(89, 21, 18),
(90, 21, 20),
(91, 21, 20),
(92, 21, 19),
(93, 21, 26),
(94, 21, 18),
(95, 21, 18),
(96, 21, 17),
(97, 21, 20),
(98, 21, 22),
(99, 21, 18),
(100, 21, 24),
(101, 21, 20),
(102, 21, 22),
(103, 21, 21),
(104, 21, 19),
(105, 21, 20),
(106, 21, 22),
(107, 21, 18),
(108, 21, 15),
(109, 22, 14),
(110, 22, 13),
(111, 22, 15),
(112, 22, 12),
(113, 22, 15),
(114, 22, 13),
(115, 22, 12),
(116, 22, 15),
(117, 22, 14),
(118, 22, 13),
(119, 22, 12),
(120, 22, 11),
(121, 22, 13),
(122, 22, 18),
(123, 22, 13),
(124, 22, 17),
(125, 22, 15),
(126, 22, 13),
(127, 22, 13),
(128, 22, 12),
(129, 22, 14),
(136, 23, 14),
(137, 23, 12),
(138, 23, 13),
(139, 23, 13),
(140, 23, 14),
(141, 23, 13),
(142, 23, 14),
(143, 23, 15),
(144, 23, 13),
(145, 23, 13),
(146, 23, 12),
(147, 23, 12),
(148, 23, 12),
(149, 23, 13),
(150, 23, 13),
(151, 23, 12),
(152, 23, 12),
(153, 23, 15),
(154, 23, 13),
(155, 23, 15),
(156, 23, 14),
(157, 23, 18),
(158, 23, 16),
(159, 23, 15),
(160, 23, 14),
(161, 23, 17),
(162, 24, 14),
(163, 24, 13),
(164, 24, 11),
(165, 24, 14),
(166, 24, 11),
(167, 24, 13),
(168, 24, 13),
(169, 24, 14),
(170, 24, 11),
(171, 24, 14),
(172, 24, 13),
(173, 24, 11),
(174, 24, 13),
(175, 24, 12),
(176, 24, 13),
(177, 24, 14),
(178, 24, 12),
(179, 24, 12),
(180, 25, 10),
(181, 25, 12),
(182, 25, 12),
(183, 25, 13),
(184, 25, 13),
(185, 25, 12),
(186, 25, 12),
(187, 25, 10),
(188, 25, 11),
(189, 25, 10),
(190, 25, 13),
(191, 25, 11),
(192, 25, 11),
(193, 25, 12),
(194, 25, 10),
(195, 25, 12),
(196, 25, 10),
(197, 25, 12),
(198, 25, 10),
(199, 25, 12),
(200, 25, 10),
(201, 26, 13),
(202, 26, 13),
(203, 26, 12),
(204, 26, 11),
(205, 26, 12),
(206, 26, 12),
(207, 26, 11),
(208, 26, 12),
(209, 26, 13),
(210, 26, 13),
(211, 26, 12),
(212, 26, 12),
(213, 26, 12),
(214, 26, 12),
(215, 26, 13),
(216, 26, 11),
(217, 26, 12),
(218, 26, 12),
(219, 26, 13),
(220, 26, 12),
(221, 26, 12),
(222, 26, 13),
(223, 26, 11),
(224, 27, 10),
(225, 27, 10),
(226, 27, 8),
(227, 27, 9),
(228, 27, 10),
(229, 27, 10),
(230, 27, 12),
(231, 27, 9),
(232, 27, 10),
(233, 27, 8),
(234, 27, 9),
(235, 27, 10),
(236, 27, 12),
(237, 27, 9),
(238, 27, 9),
(239, 27, 13),
(240, 27, 10),
(241, 28, 7),
(242, 28, 8),
(243, 28, 9),
(244, 28, 7),
(245, 28, 7),
(246, 28, 9),
(247, 28, 9),
(248, 28, 7),
(249, 28, 8),
(250, 28, 12),
(251, 28, 7),
(252, 28, 8),
(253, 28, 8),
(254, 28, 11),
(255, 29, 11),
(256, 29, 9),
(257, 29, 9),
(258, 29, 10),
(259, 29, 11),
(260, 29, 10),
(261, 29, 14),
(262, 29, 11),
(263, 29, 11),
(264, 29, 13),
(265, 29, 10),
(266, 30, 13),
(267, 30, 13),
(268, 30, 12),
(269, 30, 14),
(270, 30, 14),
(271, 30, 14),
(272, 30, 13),
(273, 30, 14),
(274, 31, 5),
(275, 31, 5),
(276, 31, 6),
(277, 31, 6),
(278, 31, 6),
(279, 31, 5),
(280, 31, 4),
(281, 31, 5),
(282, 31, 7),
(283, 32, 18),
(284, 32, 20),
(285, 32, 22),
(286, 32, 22),
(287, 32, 18),
(288, 32, 22),
(289, 32, 20),
(290, 33, 43),
(291, 33, 41),
(292, 33, 42),
(293, 33, 43),
(294, 33, 43),
(295, 33, 42),
(296, 33, 41),
(297, 33, 43),
(298, 33, 41),
(299, 33, 46),
(300, 34, 31),
(301, 34, 32),
(302, 34, 32),
(303, 34, 31),
(304, 34, 32),
(305, 34, 32),
(306, 34, 31),
(307, 34, 31),
(308, 34, 31),
(309, 34, 32),
(310, 34, 33),
(311, 34, 38),
(312, 35, 15),
(313, 35, 15),
(314, 35, 14),
(315, 35, 13),
(316, 35, 15),
(317, 36, 40),
(318, 36, 40),
(319, 36, 39),
(320, 36, 41),
(321, 36, 39),
(322, 36, 42),
(323, 36, 40),
(324, 36, 40),
(325, 36, 45),
(326, 36, 41),
(327, 36, 45),
(328, 36, 40),
(329, 36, 43),
(330, 37, 39),
(331, 37, 39),
(332, 37, 38),
(333, 37, 38),
(334, 37, 41),
(335, 37, 39),
(336, 37, 37),
(337, 37, 37),
(338, 37, 40),
(339, 38, 46),
(340, 38, 46),
(341, 38, 46),
(342, 38, 47),
(343, 38, 45),
(344, 38, 46),
(345, 38, 47),
(346, 38, 47),
(347, 38, 47),
(348, 38, 46),
(349, 38, 47),
(350, 38, 45),
(351, 38, 46),
(352, 38, 45),
(353, 38, 47),
(354, 38, 45),
(355, 38, 45),
(356, 39, 17),
(357, 39, 16),
(358, 39, 17),
(359, 39, 16),
(360, 39, 15),
(361, 39, 16),
(362, 39, 15),
(363, 39, 17),
(364, 39, 17),
(365, 39, 16),
(366, 39, 17),
(367, 39, 17),
(368, 39, 16),
(369, 39, 16),
(370, 40, 16),
(371, 40, 15),
(372, 40, 16),
(373, 40, 15),
(374, 40, 15),
(375, 40, 18),
(376, 40, 16),
(377, 40, 16),
(378, 40, 17),
(379, 40, 14),
(380, 40, 16);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `akses_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `nama`, `akses_id`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `centroid`
--
ALTER TABLE `centroid`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bahan_id` (`barang_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `akses_id` (`akses_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `akses`
--
ALTER TABLE `akses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `centroid`
--
ALTER TABLE `centroid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=381;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`akses_id`) REFERENCES `akses` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
include('../config/settings.php');

session_start();
if (!isset($_SESSION['akses'])){
	header('location: ../public/error/500.php');
}
else{
	// $base_dir = $_SESSION['base_dir'];
?>
<html>
<head>
	<title>Admin's Page</title>
	<link rel="stylesheet" type="text/css" href="../public/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../public/custom/style.css">
</head>
<body class="admin-page-body">
	<?php
	include "navbar.php";
	?>
	<div class="shadow-page">
		<div class="basic-page">
			<h3>Ubah Data Bahan</h3>
			<form action="." method="POST">
			<table class="table">
			<?php
			$q_data = mysqli_query($connection, 'select * from transaksi where id='.$_GET['id']);
			while($row=mysqli_fetch_array($q_data)){
			?>
			<input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
			<input type="hidden" name="update" value="update">
			<tr>
				<td width="30%">Nama Barang</td>
				<td>:</td>
				<td width="69%">
					<select name="barang" class="form-control" required=true>
						<?php
							$barang = mysqli_query($connection, "select * from barang");
							$i = 1;
							while($r=mysqli_fetch_array($barang)){
								$id = $r['id'];
								$nama_barang = $r['nama'];
								if ($id == $row['barang_id']){
									echo "<option value='$id' selected>$nama_barang</option>";
								}
								else{
									echo "<option value='$id'>$nama_barang</option>";	
								}
								$i++;
							}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td width="30%">Jumlah</td>
				<td>:</td>
				<td width="69%"><input type="text" name="jumlah" class="form-control" maxlength="150" value="<?php echo $row['jumlah'];?>"></td>
			</tr>
			<?php
				}
			?>
			<tr>
				<td colspan="3">
					<input type="submit" class="btn btn-success" style="float:right; margin:2px;" value="Ubah">
					<a href="bahan.php" class="btn btn-success" style="float:right; margin:2px;">Kembali</a>
				</td>
			</tr>
			</table>
			</form>
		<div>
	</div>
</body>
</html>
<?php } ?>
<?php
include('../config/settings.php');
include('function.php');

session_start();
if (!isset($_SESSION['akses'])){
	header('location: ../public/error/500.php');
}
else{
?>
<html>
<head>
	<title>Admin's Page</title>
	<link rel="stylesheet" type="text/css" href="../public/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../public/custom/style.css">
</head>
<body class="admin-page-body">
	<?php
		include "navbar.php";
	?>	
	<div class="shadow-page">
		<div class="basic-page">
			<?php
				if(isset($_POST['insert'])){
					$barang =  $_POST['barang'];
					$jumlah =  $_POST['jumlah'];
					$simpan = mysqli_query($connection, "INSERT INTO transaksi (`id`, `barang_id`, `jumlah`) VALUES ('', '$barang', '$jumlah')");
					if (!$simpan) {
						die("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a>"."Data gagal tersimpan <br>Kode Error : " . mysqli_error()."</div>");
					}
					else{
						($simpan);
						echo "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a> Data sudah tersimpan </div>";
					}
				}
				else if(isset($_POST['update'])){
					$id =  $_POST['id'];
					$barang =  $_POST['barang'];
					$jumlah =  $_POST['jumlah'];
					$update = mysqli_query($connection, "UPDATE transaksi SET barang_id='$barang', jumlah='$jumlah' WHERE id='$id' ");
					if (!$update) {
						die("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a>"."Data gagal diubah <br>Kode Error : " . mysqli_error()."</div>");
					}
					else{
						($update);
						echo "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a> Data berhasil diubah </div>";
					}
				}
				else if(isset($_POST['delete'])){
					$id =  $_POST['id'];
					$delete = mysqli_query($connection, "DELETE from transaksi WHERE id='$id' ");
					if (!$delete) {
						die("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a>"."Data gagal dihapus <br>Kode Error : " . mysqli_error()."</div>");
					}
					else{
						($delete);
						echo "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a> Data berhasil dihapus </div>";
					}
				}
			?>
			<h3>
				Data Transaksi
			</h3>
			<form action="" method="POST">
				<input type="hidden" name="insert">
				<table class="table">
					<tr>
						<td width="30%">Nama Barang</td>
						<td>:</td>
						<td width="69%">
							<select name="barang" class="form-control">
								<option>---</option>
								<?php
								$barang = mysqli_query($connection, "select * from barang");
								$i = 1;
								while($row=mysqli_fetch_array($barang)){
									$id = $row['id'];
									$nama_barang = $row['nama'];
									echo "<option value='$id'>$nama_barang</option>";
									$i++;
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td width="30%">Jumlah Barang</td>
						<td>:</td>
						<td width="69%">
							<input type="number" name="jumlah" class="form-control">
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<input type="submit" class="btn btn-success" style="float:right;" value="Simpan">
						</td>
					</tr>
				</table>
			</form>
			<br>
			<h4>Detail Transaksi</h4>
			<table class="table table-hover table-stripped">
				<tr>
					<th>No</th>
					<th>Nama Barang</th>
					<th>Jumlah</th>
					<th>Tindakan</th>
				</tr>
				<?php
					$transaksi = mysqli_query($connection, "select * from transaksi");
					$i = 1;
					while ($row = mysqli_fetch_array($transaksi)) {
					?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo namaBarang($row['barang_id']); ?></td>
						<td><?php echo $row['jumlah']; ?></td>
						<td>
							<a class="btn btn-info" href="<?php echo "edit_transaksi.php?id=".$row['id']; ?>">Edit</a>
							<a class="btn btn-danger" href="<?php echo "hapus_transaksi.php?id=".$row['id']; ?>">Hapus</a>
						</td>
					</tr>
					<?php
						$i++;
					}
				?>

			</table>
		<div>
	</div>
</body>
</html>
<?php } ?>
<?php
function euclidean($data1, $data2){
if(sizeof($data1) == sizeof($data2)){
  $result = 0;
  for ($i=0; $i < sizeof($data1); $i++) { 
   $result += pow(($data1[$i] - $data2[$i]), 2);
  }
  $result = sqrt($result);
 }
return $result;
}

function minNumber($value, $arr){
	if(min($arr) == $value){
		$res = min($arr);
	}
	else{
		$res = "-";
	}
	return $res;
}

function indexNumber($arr){
	$min = min($arr);
	for ($i=0; $i < sizeof($arr); $i++) { 
		if($min == $arr[$i]){
			$res = $i;
		}
	}
	return $res +1;
}

function viewVolume(){
	include '../config/settings.php';
	$data = array();
	$barang = array();
	$jumlah = array();
	$volume = array();
	$view = mysqli_query($connection, "select distinct barang_id from transaksi order by barang_id asc");
	$i = 1;
	while($row=mysqli_fetch_array($view)){
		$barang_id = $row['barang_id'];
		$brg = mysqli_query($connection, "select nama from barang where id = $barang_id");
		$brg = mysqli_fetch_all($brg, MYSQLI_ASSOC);

		$qjumlah = mysqli_query($connection, "select * from transaksi where barang_id = $barang_id ");
		$jml = mysqli_num_rows($qjumlah);
		$vol = 0;
		while($r=mysqli_fetch_array($qjumlah)){
			$vol += $r['jumlah'];
		}
		array_push($barang, $barang_id);
		array_push($jumlah, $jml);
		array_push($volume, $vol);
		$i++;
	}
	$data['barang'] = $barang;
	$data['jumlah'] = $jumlah;
	$data['volume'] = $volume;
	return $data;
}

function namaBarang($id){
	include '../config/settings.php';
	$brg = "";
	$barang = mysqli_query($connection, "select nama from barang where id = $id");
	$barang = mysqli_fetch_all($barang, MYSQLI_ASSOC);
	$brg = $barang[0]['nama'];
	return $brg;
}

function tableCentroid($centroid, $index){
	$title = "<h4>Centroid Index ke-".($index+1)."</h4>";
	$table = $title."<table class='table table-stripped table-hover'>";
	$key = ['nama', 'jumlah', 'volume', 'rerata'];
	for ($i=0; $i < count($centroid[$index]); $i++) { 
		$table = $table."<tr>";
		for ($j=0; $j < count($centroid[$index][$i]); $j++) { 
			$table = $table."<td>";	
			$table = $table.$centroid[$index][$i][$key[$j]];
			$table = $table."</td>";	
		}
		$table = $table."</tr>";
	}
	$table = $table."</table>";
	return $table;
}
function tableEuclid($centroid, $index, $barang){
	$title = "<h5>Tabel Euclidean data dengan Centroid ke-".($index+1)."</h5>";
	$table = $title."<table class='table table-stripped table-hover'>";

	$key = ['nama', 'jumlah', 'volume', 'rerata'];
	$table = $table."<tr style='text-transform:uppercase;'>";
	for ($i=0; $i < count($key); $i++) { 
		$table = $table."<th>".$key[$i]."</th>";
	}
	$table = $table."</tr>";
	for ($i=0; $i < count($centroid[$index]); $i++) { 
		$table = $table."<tr>";
		for ($j=0; $j < count($centroid[$index][$i]); $j++) { 
			$table = $table."<td>";	
			$table = $table.$centroid[$index][$i][$key[$j]];
			$table = $table."</td>";	
		}
		$table = $table."</tr>";
	}

	$table = $table."</table>";
	return $table;
}
<?php
include('../config/settings.php');

session_start();
if (!isset($_SESSION['akses'])){
	header('location: ../public/error/500.php');
}
else{
?>
<html>
<head>
	<title>Admin's Page</title>
	<link rel="stylesheet" type="text/css" href="../public/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../public/custom/style.css">
</head>
<body class="admin-page-body">
	<?php
		include "navbar.php";
		include "function.php";
	?>	
	<div class="shadow-page">
		<div class="basic-page">
		<?php
			$centroid = [];
			$q_barang = mysqli_query($connection, 'select * from centroid');
			$i = 1;
			while($row=mysqli_fetch_array($q_barang)){
			array_push($centroid, [$row['jumlah'], $row['volume'], $row['rerata']]);
			$i++;
			}
			$end = 0;
			$counter = 1;
			while ($end != 1) {	
			$volume = viewVolume();
			for ($i=0; $i < count($volume['barang']); $i++) {
				$data = [$volume['jumlah'][$i], $volume['volume'][$i], ($volume['volume'][$i] / $volume['jumlah'][$i])];
			}
			for ($i=0; $i < count($volume['barang']); $i++) { 
			$data = [$volume['jumlah'][$i], $volume['volume'][$i], ($volume['volume'][$i] / $volume['jumlah'][$i]) ];
			$arr = [euclidean($centroid[0], $data), euclidean($centroid[1], $data), euclidean($centroid[2], $data)];
			}
				$jumlah_centroid1 = 0;
				$volume_centroid1 = 0;
				$rerata_centroid1 = 0;
				$kuantitas_centroid1 = 0;

				for ($i=0; $i < count($volume['barang']); $i++) { 
				$data = [$volume['jumlah'][$i], $volume['volume'][$i], ($volume['volume'][$i] / $volume['jumlah'][$i]) ];
				$arr = [euclidean($centroid[0], $data), euclidean($centroid[1], $data), euclidean($centroid[2], $data)];
				if(minNumber(euclidean($centroid[0], $data), $arr) != "-"){

					$kuantitas_centroid1++;
					$jumlah_centroid1 = $jumlah_centroid1 + $volume['jumlah'][$i];
					$volume_centroid1 = $volume_centroid1 + $volume['volume'][$i];
					$rerata_centroid1 = $rerata_centroid1 + ($volume['volume'][$i] / $volume['jumlah'][$i]);
					}
				}

				$jumlah_centroid1 = $jumlah_centroid1/$kuantitas_centroid1;
				$volume_centroid1 = $volume_centroid1/$kuantitas_centroid1;
				$rerata_centroid1 = $rerata_centroid1/$kuantitas_centroid1;
				$q_barang = mysqli_query($connection, 'select * from barang');
				$i = 1;

				$jumlah_centroid2 = 0;
				$volume_centroid2 = 0;
				$rerata_centroid2 = 0;
				$kuantitas_centroid2 = 0;

				for ($i=0; $i < count($volume['barang']); $i++) { 
				$data = [$volume['jumlah'][$i], $volume['volume'][$i], ($volume['volume'][$i] / $volume['jumlah'][$i]) ];
				$arr = [euclidean($centroid[0], $data), euclidean($centroid[1], $data), euclidean($centroid[2], $data)];
				if(minNumber(euclidean($centroid[1], $data), $arr) != "-"){

					$kuantitas_centroid2++;
					$jumlah_centroid2 = $jumlah_centroid2 + $volume['jumlah'][$i];
					$volume_centroid2 = $volume_centroid2 + $volume['volume'][$i];
					$rerata_centroid2 = $rerata_centroid2 + ($volume['volume'][$i] / $volume['jumlah'][$i]);
					}
				}

				$jumlah_centroid2 = $jumlah_centroid2/$kuantitas_centroid2;
				$volume_centroid2 = $volume_centroid2/$kuantitas_centroid2;
				$rerata_centroid2 = $rerata_centroid2/$kuantitas_centroid2;
				$q_barang = mysqli_query($connection, 'select * from barang');
				$i = 1;

				$jumlah_centroid3 = 0;
				$volume_centroid3 = 0;
				$rerata_centroid3 = 0;
				$kuantitas_centroid3 = 0;

				for ($i=0; $i < count($volume['barang']); $i++) { 	
				$data = [$volume['jumlah'][$i], $volume['volume'][$i], ($volume['volume'][$i] / $volume['jumlah'][$i]) ];
				$arr = [euclidean($centroid[0], $data), euclidean($centroid[1], $data), euclidean($centroid[2], $data)];
				if(minNumber(euclidean($centroid[2], $data), $arr) != "-"){
					$kuantitas_centroid3++;
					$jumlah_centroid3 = $jumlah_centroid3 + $volume['jumlah'][$i];
					$volume_centroid3 = $volume_centroid3 + $volume['volume'][$i];
					$rerata_centroid3 = $rerata_centroid3 + ($volume['volume'][$i] / $volume['jumlah'][$i]);
					}
				}

				$jumlah_centroid3 = $jumlah_centroid3/$kuantitas_centroid3;
				$volume_centroid3 = $volume_centroid3/$kuantitas_centroid3;
				$rerata_centroid3 = $rerata_centroid3/$kuantitas_centroid3;

			$centroid2 = [];
			array_push($centroid2, [$jumlah_centroid1, $volume_centroid1, $rerata_centroid1]);
			array_push($centroid2, [$jumlah_centroid2, $volume_centroid2, $rerata_centroid2]);
			array_push($centroid2, [$jumlah_centroid3, $volume_centroid3, $rerata_centroid3]);
			if ($centroid2 == $centroid) {
				$end = 1;
			}
			else{
				$centroid = $centroid2;
			}
			$counter++;

			if($counter == 50){
				$end = 1;
			}
		}
		?>
		<h4>Tabel Hasil K-Means Clustering</h4>
		<table class="table">
			<tr>
				<th>No</th>
				<th>Kluster</th>
				<th>Jumlah</th>
				<th>Volume</th>
				<th>Rata-rata</th>
			</tr>
			<?php
				for ($i=0; $i < count($centroid); $i++) {
					?>
					<tr>
						<td><?php echo $i+1;?></td>
						<td>C<?php echo $i+1;?></td>
						<td><?php echo $centroid[$i][0]?></td>
						<td><?php echo $centroid[$i][1]?></td>
						<td><?php echo $centroid[$i][2]?></td>
					</tr>
			<?php
				}
			?>
		</table>
Cluster 1
			<table class="table table-hover table-stripped">
				<tr>
					<th>No</th>
					<th>Nama Barang</th>
					<th>Jumlah Barang</th>
					<th>Volume Barang</th>
					<th>Rata-rata</th>
				</tr>
				<?php
				$jumlah_centroid1 = 0;
				$volume_centroid1 = 0;
				$rerata_centroid1 = 0;
				$kuantitas_centroid1 = 0;

				for ($i=0; $i < count($volume['barang']); $i++) { 
				$data = [$volume['jumlah'][$i], $volume['volume'][$i], ($volume['volume'][$i] / $volume['jumlah'][$i]) ];
				$arr = [euclidean($centroid[0], $data), euclidean($centroid[1], $data), euclidean($centroid[2], $data)];
				if(minNumber(euclidean($centroid[0], $data), $arr) != "-"){

					$kuantitas_centroid1++;
					$jumlah_centroid1 = $jumlah_centroid1 + $volume['jumlah'][$i];
					$volume_centroid1 = $volume_centroid1 + $volume['volume'][$i];
					$rerata_centroid1 = $rerata_centroid1 + ($volume['volume'][$i] / $volume['jumlah'][$i]);

					?>
					<tr>
						<td><?php echo $i+1;?></td>
						<td><?php echo namaBarang($volume['barang'][$i])?></td>
						<td><?php echo $volume['jumlah'][$i]?></td>
						<td><?php echo $volume['volume'][$i]?></td>

						<td><?php echo $volume['volume'][$i] / $volume['jumlah'][$i] ?></td>
					</tr>
					<?php
					}
				}

				$jumlah_centroid1 = $jumlah_centroid1/$kuantitas_centroid1;
				$volume_centroid1 = $volume_centroid1/$kuantitas_centroid1;
				$rerata_centroid1 = $rerata_centroid1/$kuantitas_centroid1;

			?>
			</table>
Cluster 2
			<table class="table table-hover table-stripped">
				<tr>
					<th>No</th>
					<th>Nama Barang</th>
					<th>Jumlah Barang</th>
					<th>Volume Barang</th>
					<th>Rata-rata</th>
				</tr>
				<?php
				$q_barang = mysqli_query($connection, 'select * from barang');
				$i = 1;

				$jumlah_centroid2 = 0;
				$volume_centroid2 = 0;
				$rerata_centroid2 = 0;
				$kuantitas_centroid2 = 0;

				for ($i=0; $i < count($volume['barang']); $i++) { 
				$data = [$volume['jumlah'][$i], $volume['volume'][$i], ($volume['volume'][$i] / $volume['jumlah'][$i]) ];
				$arr = [euclidean($centroid[0], $data), euclidean($centroid[1], $data), euclidean($centroid[2], $data)];
				if(minNumber(euclidean($centroid[1], $data), $arr) != "-"){

					$kuantitas_centroid2++;
					$jumlah_centroid2 = $jumlah_centroid2 + $volume['jumlah'][$i];
					$volume_centroid2 = $volume_centroid2 + $volume['volume'][$i];
					$rerata_centroid2 = $rerata_centroid2 + ($volume['volume'][$i] / $volume['jumlah'][$i]);

					?>
					<tr>
						<td><?php echo $i+1;?></td>
						<td><?php echo namaBarang($volume['barang'][$i])?></td>
						<td><?php echo $volume['jumlah'][$i]?></td>
						<td><?php echo $volume['volume'][$i]?></td>
						<td><?php echo $volume['volume'][$i] / $volume['jumlah'][$i] ?></td>
					</tr>
					<?php
					}
				}

				$jumlah_centroid2 = $jumlah_centroid2/$kuantitas_centroid2;
				$volume_centroid2 = $volume_centroid2/$kuantitas_centroid2;
				$rerata_centroid2 = $rerata_centroid2/$kuantitas_centroid2;

				?>
			</table>

Cluster 3
			<table class="table table-hover table-stripped">
				<tr>
					<th>No</th>
					<th>Nama Barang</th>
					<th>Jumlah Barang</th>
					<th>Volume Barang</th>
					<th>Rata-rata</th>
				</tr>
				<?php
				$q_barang = mysqli_query($connection, 'select * from barang');
				$i = 1;

				$jumlah_centroid3 = 0;
				$volume_centroid3 = 0;
				$rerata_centroid3 = 0;
				$kuantitas_centroid3 = 0;

				for ($i=0; $i < count($volume['barang']); $i++) { 	
				$data = [$volume['jumlah'][$i], $volume['volume'][$i], ($volume['volume'][$i] / $volume['jumlah'][$i]) ];
				$arr = [euclidean($centroid[0], $data), euclidean($centroid[1], $data), euclidean($centroid[2], $data)];
				if(minNumber(euclidean($centroid[2], $data), $arr) != "-"){
					$kuantitas_centroid3++;
					$jumlah_centroid3 = $jumlah_centroid3 + $volume['jumlah'][$i];
					$volume_centroid3 = $volume_centroid3 + $volume['volume'][$i];
					$rerata_centroid3 = $rerata_centroid3 + ($volume['volume'][$i] / $volume['jumlah'][$i]);
					?>
					<tr>
						<td><?php echo $i+1;?></td>
						<td><?php echo namaBarang($volume['barang'][$i])?></td>
						<td><?php echo $volume['jumlah'][$i]?></td>
						<td><?php echo $volume['volume'][$i]?></td>
						<td><?php echo $volume['volume'][$i] / $volume['jumlah'][$i] ?></td>
					</tr>
					<?php
					}
				}

				$jumlah_centroid3 = $jumlah_centroid3/$kuantitas_centroid3;
				$volume_centroid3 = $volume_centroid3/$kuantitas_centroid3;
				$rerata_centroid3 = $rerata_centroid3/$kuantitas_centroid3;

				?>
			</table>
		</div>
	</div>
</body>
</html>
<?php } ?>
<?php
include('../config/settings.php');

session_start();
if (!isset($_SESSION['akses'])){
	header('location: ../public/error/500.php');
}
else{
?>
<html>
<head>
	<title>Admin's Page</title>
	<link rel="stylesheet" type="text/css" href="../public/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../public/custom/style.css">
</head>
<body class="admin-page-body">
	<?php
		include "navbar.php";
		include "function.php";
	?>	
	<div class="shadow-page">
		<div class="basic-page">
			<h3>
				Rekap Data Transaksi
			</h3>
			<table class="table table-hover table-stripped">
				<tr>
					<th>No</th>
					<th>Nama Barang</th>
					<th>Jumlah</th>
					<th>Volume</th>
					<th>Rata-rata</th>
				</tr>
				<?php
					$data = viewVolume();
					for ($i=0; $i < count($data['volume']); $i++) { 
						?>
						<tr>
							<td><?php echo $i+1;?></td>
							<td><?php echo namaBarang($data['barang'][$i]);?></td>
							<td><?php echo $data['jumlah'][$i];?></td>
							<td><?php echo $data['volume'][$i];?></td>
							<td><?php echo ($data['volume'][$i] / $data['jumlah'][$i]);?></td>
						</tr>
						<?php
					}
				?>
			</table>
		<div>
	</div>
</body>
</html>
<?php } ?>
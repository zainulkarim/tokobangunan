<?php
include('../config/settings.php');

session_start();
if (!isset($_SESSION['akses'])){
	header('location: ../public/error/500.php');
}
else{
	// $base_dir = $_SESSION['base_dir'];
?>
<html>
<head>
	<title>Admin's Page</title>
	<link rel="stylesheet" type="text/css" href="../public/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../public/custom/style.css">
</head>
<body class="admin-page-body">
	<?php
		include 'navbar.php';
	?>
	<div class="shadow-page">
		<div class="basic-page">
			<?php
				if(isset($_POST['insert'])){
					$nama =  $_POST['nama'];
					$simpan = mysqli_query($connection, "INSERT INTO barang (`id`, `nama`) VALUES ('', '$nama')");
					if (!$simpan) {
						die("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a>"."Data gagal tersimpan <br>Kode Error : " . mysqli_error($connection)."</div>");
					}
					else{
						($simpan);
						echo "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a> Data sudah tersimpan </div>";
					}
				}
				else if(isset($_POST['update'])){
					$id =  $_POST['id'];
					$nama = $_POST['nama'];
					$update = mysqli_query($connection, "UPDATE barang SET nama='$nama' WHERE id='$id' ");
					if (!$update) {
						die("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a>"."Data gagal diubah <br>Kode Error : " . mysqli_error($connection)."</div>");
					}
					else{
						($update);
						echo "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a> Data berhasil diubah </div>";
					}
				}
				else if(isset($_POST['delete'])){
					$id =  $_POST['id'];
					$delete = mysqli_query($connection, "DELETE from barang WHERE id='$id' ");
					if (!$delete) {
						die("<div class='alert alert-danger alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a>"."Data gagal dihapus <br>Kode Error : " . mysqli_error($connection)."</div>");
					}
					else{
						($delete);
						echo "<div class='alert alert-success alert-dismissable'><a href='#' class='close' data-dismiss='alert' aria-label='close'>x</a> Data berhasil dihapus </div>";
					}
				}
			?>
			<h3>Data barang</h3>
			<form action="" method="POST">
			<input type="hidden" name="insert" value="insert">
			<table class="table">
			<tr>
				<td width="30%">Nama Barang</td>
				<td>:</td>
				<td width="69%"><input type="text" name="nama" class="form-control" maxlength="150"></td>
			</tr>
			<tr>
				<td colspan="3">
					<input type="submit" class="btn btn-success" style="float:right;" value="Simpan">
				</td>
			</tr>
			</table>
			</form>
			<table class="table table-hover table-stripped">
				<tr>
					<th>No</th>
					<th>Nama Barang</th>
					<th>Tindakan</th>
				</tr>
				<?php
				$q_barang = mysqli_query($connection, 'select * from barang');
				$i = 1;
				while($row=mysqli_fetch_array($q_barang)){
				?>
				<tr>
					<td><?php echo $i;?></td>
					<td><?php echo $row['nama']?></td>
					<td>
						<a class="btn btn-info" href="<?php echo "edit_bahan.php?id=".$row['id']; ?>">Edit</a>
						<a class="btn btn-danger" href="<?php echo "hapus_bahan.php?id=".$row['id']; ?>">Hapus</a>
					</td>
				</tr>
				<?php
				$i++;
				}
				?>
			</table>
		<div>
	</div>
</body>
</html>
<?php } ?>